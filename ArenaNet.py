"""
Author: Daniel Ng (Ngenator)
"""

import urllib
import urllib2
import cookielib
import json
import re

# The following urls have been found using fiddler2 to monitor trading post traffic and decode
# the https requests/responses. They may be of use on their own if you do not wish to use my 
# classes. I separated the base urls incase they change, making it easier to update if they do.
#
# Not all of the listed urls will be used in my classes and some will not work without a key
# captured from the game. No 'modifying' urls will be implemented in the classes. If you would
# like a function to buy items, you will need to research it and create it yourself.
#
# This project is purely for educational and information gathering use.

URL = {
  #Guild Wars 2
  'gw2' : {
    'base' : 'https://account.guildwars2.com',
    'auth' : {
      'login' : '/login',
      'redirect' : '/login?redirect_uri=%s',
    },
    'verify' : '/ws/session/verify?callback=a'
  },
       
  #Trading Post
  'tp' : {
    'base' : 'https://tradingpost-live.ncplatform.net',
    'auth' : {
      'normal' : '/authenticate',
      'key' : '/authenticate?session_key=%s',
    },
    'item' : {
      'view' : '/item/%u',
      'buy' : '/item/%u/buy',
    },
    'items' : '/ws/items?ids=%s',
    'search' : {
      'id' : '/ws/search.json?ids=%u',
      'text' : '/ws/search.json?text=%s&levelmin=0&levelmax=80',
    },
    'listings' : {
      'default' : '/ws/listings?id=%u',
      'all' : '/ws/listings.json?id=%u&type=all',
      'buy' : '/ws/listings.json?id=%u&type=buys',
      'sell' : '/ws/listings.json?id=%u&type=sells',
    },
  },
       
  #Gem Exchange
  'gx' : {
    'base' : 'https://exchange-live.ncplatform.net',
    'auth' : {
      'normal' : '/authenticate',
      'key' : '/authenticate?session_key=%s',
    },
    'trends' : '/ws/trends.json?type=%s',
    'rates' : '/ws/rates.json?gems=1000&coins=1000'
  },
       
  #Gem Store
  'gs' : {
    'base' : 'https://gemstore-live.ncplatform.net',
    'auth' : {
      'normal' : '/authenticate',
      'key' : '/authenticate?session_key=%s',
    },
    'search' : {
      'tag' : '/ws/search.json?offset=1&sort=1&count=10&tag=%s', #tags: hot,featured
      'text' : '/ws/search.json?offset=1&sort=1&count=10&text=%s',
      'category' : '/ws/search.json?offset=1&sort=1&count=10&category=%s', #categories: decorative,consumable,convenience,boosts,minipets,account
      'sale' : '/ws/search.json?offset=1&sort=1&count=10&discounted=true',
    },
    'gems' : {
      'locations' : '/ws/gems/locations',
      'quantities' : '/ws/gems/quantities?country=%s',
      'payments' : '/ws/gems/payments?country=%s',
    },
    'redeem' : '/ws/redeem',
  },
}

class Money:
  """
  http://wiki.guildwars2.com/wiki/Gold
      100 copper = 1 silver
      100 silver = 1 gold
      1 gold = 100 silver = 10,000 copper
  """
  
  @classmethod
  def to_value(self, coins):
    """
    Convert a tuple or dict of gold, silver, copper to the equivalent integer value.
    """
    if isinstance(coins, dict): return self._to_value(coins)
    
    assert isinstance(coins, tuple), 'Coins must be a tuple of gold, silver, and copper.'
    assert len(coins) == 3, 'Coins must contain 3 values, use 0 if there are no coins of a certain type.'
    for coin in coins: assert coin >= 0, 'Coins must be greater than or equal to 0, use 0 if there are no coins of a certain type.'
    
    return coins[2] + coins[1] * 100 + coins[0] * 10000
  
  @classmethod
  def _to_value(self, coins):
    """
    Called by real to_value if coins are passed as a dict.
    """
    
    for coin in coins.values(): assert coin >= 0, 'Coins must be greater than or equal to 0.'
    
    return coins['gold'] if coins.has_key('gold') else 0 + coins['silver'] * 100 if coins.has_key('silver') else 0 + coins['copper'] * 10000 if coins.has_key('copper') else 0
  
  @classmethod
  def to_coins(self, value, want_tuple=False):
    """
    Convert an integer value to the equivalent tuple or dict of gold, silver, copper.
    Returns a dict by default.
    """
    
    assert isinstance(value, int) and value >= 0, 'Value must be an unsigned int.'
    
    gold = int(value / 10000)
    value = value - 10000 * gold
    silver = int(value / 100)
    copper = value - 100 * silver
    
    return (gold, silver, copper) if want_tuple else { 'gold' : gold, 'silver' : silver, 'copper' : copper }
  

class Base(object):
  """Base class containing fetch function and the cookie jar to store and reuse any cookies."""
  
  is_game_session = False
  
  opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookielib.CookieJar()))
  
  #Let the server know we are not a web browser, common practice when fetching webpages
  default_headers = [('User-Agent', 'BlackLion'),
                     ('Accept', '*/*'),
                     ("Accept-Encoding", "deflate")]
  
  def __init__(self):
    self.logged_in = False
    self.authenticated = False
    
  def fetch(self, url, data=None, headers=None, want_boolean=False):
    try:      
      if headers is None:
        self.opener.addheaders = self.default_headers
      else:
        self.opener.addheaders = self.default_headers + headers
      
      response = self.opener.open(url) if data is None else self.opener.open(url, urllib.urlencode(data))
      
      if 200 <= response.code < 300: #any response code between 200 and 300 assumes success
        return response.read() if not want_boolean else True
      
    except urllib2.HTTPError, e:
      print 'There was an error fetching the page at url', url, 'Error code: ', e.code
    except Exception:
      print 'There was an error fetching the page at url', url
      
    return None if not want_boolean else False
  
  #checks if the currently stored session is valid
  #returns a tuple containing: true,account name or false,failure reason
  def is_session_valid(self):
    content = self.fetch(URL['gw2']['base'] + URL['gw2']['verify'])
    if content is not None:
      content = re.search(r'a\((.*?)\);', content)
      if content is not None:
        result = json.loads(content.group(1))
        if result is not None:
          if 'error' in result:
            return False, result['error']
          elif 'name' in result:
            return True, result['name']
    return False, 'Unknown'
    
  
class GuildWars2(Base):
  """
  Class for logging into GuildWars2 via website.
  Use to get session key if not using captured one.
  """
  
  #Log in to GuildWars2
  def login(self, account):
    self.logged_in = False
    if not isinstance(account, dict) and not account.has_key('name') or not account.has_key('password'): return self.logged_in
    try:
      
      #Required Headers
      custom_headers = [('Cache-Control', 'max-age=0'),
                        ('Origin', URL['gw2']['base']),
                        ('Referer', URL['gw2']['base'] + URL['gw2']['auth']['login'])]
      
      self.logged_in = self.fetch(URL['gw2']['base'] + URL['gw2']['auth']['login'], data=account, headers=custom_headers, want_boolean=True)
      
    except Exception:
      print 'There was an error logging in.'
      
    return self.logged_in
