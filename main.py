"""
Author: Daniel Ng (Ngenator)
"""

from BlackLion import *

from config import account

if __name__ == '__main__':
  gw = GuildWars2()
  print 'Logged in? %s' % gw.login(account['login'])
  print gw.is_session_valid() #need this check incase login fails since it still returns true if the request is valid
  
  tp = TradingPost()
  print 'TradingPost Authenticated?', tp.authenticate()
  print '\tId search:', tp.search_id(20323)
#  print '\tText search:', tp.search_text('Unidentified Dye')
#  print '\tItem listing:', tp.listings(20323)
  
#  ex = GemExchange()
#  print 'GemExchange Authenticated?', ex.authenticate()
#  print '\tTrends ReceivingCoins:', ex.trends()
#  print '\tTrends ReceivingGems:', ex.trends(gems=True)
#  print '\tRates:', ex.rates()
#  
#  print Convert.to_value((11,22,33))
#  print Convert.to_coins(112233)


"""
Item Info
{ 
  u'data_id': u'20323', 
  u'name': u'Unidentified Dye'
  u'description': u'Double-click to identify one random dye color which can be unlocked for one of your characters or recycled at the Mystic Forge.', 
  u'img': u'https://dfach8bufmqqv.cloudfront.net/gw2/img/content/94fc1bc1.png',
  u'level': u'0',
  u'rarity_word': u'Basic', 
  u'rarity': u'1', 
  u'vendor_sell_price': u'0', 
  u'min_sale_unit_price': u'400', 
  u'max_offer_unit_price': u'380',
  u'sale_availability': u'66253', 
  u'offer_availability': u'130485', 

  u'count': u'66253', 
  u'gem_store_description': u'Each random dye box contains a dye unlock for your character to use as often as they like to color armor and town clothes.', 
  u'price': u'400', 
  u'type_id': u'3', 
  u'restriction_level': u'0', 
  u'gem_store_blurb': u'Customize your characters with these unlockable dyes.', 
  u'vendor': u'0', 
}
"""