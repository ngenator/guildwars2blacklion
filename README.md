#Guild Wars 2 Black Lion

Classes to access some features of the Guild Wars 2 Black Lion trading post, gem exchange and gem store.  

**Note:** Not all features work without capturing a session_key from the game itself.  

###TradingPost 
* `authenticate(session_key=None)` Authenticate the current stored (in cookies) session or use the provided one.
* `search_id(item_id)` Search for and return a dict of information about the item based off its id or None if failure or not found.
* `search_text(item_text)` Search for and return a dict of information about any matching items based off provided text.

#Copyright and License

Copyright (c) 2012 Daniel Ng

This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License](http://creativecommons.org/licenses/by-nc-sa/3.0/).