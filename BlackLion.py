"""
Author: Daniel Ng (Ngenator)
"""

import json

from ArenaNet import *


class TradingPost(Base):
  """
  Access the trading post.
  """
  
  def authenticate(self,session_key=None):
    """Authenticates using provided session key or stored session key."""
    
    self.authenticated = False
    try:
      url = URL['tp']['auth']['normal'] if session_key is None else URL['tp']['auth']['key'] % session_key
      self.authenticated = self.fetch(URL['tp']['base'] + url,want_boolean=True)
    except Exception:
      print 'There was an error authenticating.'
    return self.authenticated
  
  def search_id(self,item_id):
    """Returns dict containing json info about the item id or None if error."""
    
    if not self.authenticated: return False
    content = self.fetch(URL['tp']['base'] + URL['tp']['search']['id'] % int(item_id))
    if content is not None:
      return json.loads(content)
    return None
  
  def search_text(self,item_text):
    """Returns dict containing json info about the item or None if error."""
    
    if not self.authenticated: return False
    item_text = item_text.replace(' ','+')
    content = self.fetch(URL['tp']['base'] + URL['tp']['search']['text'] % item_text)
    if content is not None:
      return json.loads(content)
    return None
  
  def listings(self,item_id,listing_type='default'):
    """Requires game session."""
    
    if not self.authenticated or not self.is_game_session: return False
    content = self.fetch(URL['tp']['base'] + URL['tp']['listings'][listing_type] % int(item_id), headers=[("X-Requested-With","XMLHttpRequest")])
    if content is not None:
      return json.loads(content)
    return None


class GemExchange(Base):
  """
  Access the gem exchange.
  """
  
  def authenticate(self,session_key=None):
    self.authenticated = False
    try:
      url = URL['gx']['auth']['normal'] if session_key is None else URL['gx']['auth']['key'] % session_key
      self.authenticated = self.fetch(URL['gx']['base'] + url,want_boolean=True)
    except Exception:
      print 'There was an error authenticating.'
    return self.authenticated
  
  def trends(self,gems=False):
    url = URL['gx']['trends'] % 'ReceivingGems' if gems else URL['gx']['trends'] % 'ReceivingCoins'
    content = self.fetch(URL['gx']['base'] + url)
    if content is not None:
      return json.loads(content)
    return None
  
  def rates(self):
    content = self.fetch(URL['gx']['base'] + URL['gx']['rates'])
    if content is not None:
      return json.loads(content)
    return None

class GemStore(Base):
  """
  Access the gem store.
  """
  pass