import sqlite3, time, logging, sys

from BlackLion import *

from config import account


class BlackLionDatabase():
  def __init__(self, database="BlackLion.db"):
    self.logger = logging.getLogger("BlackLionDatabase")
    self.logger.setLevel(logging.DEBUG)
    fmt = logging.Formatter("%(name)s | %(asctime)s | %(levelname)-5s | %(message)s","%H:%M:%S")

    ch = logging.StreamHandler(sys.stdout)
    ch.setFormatter(fmt)
    
    self.logger.addHandler(ch)
    
    self.database = sqlite3.connect(database)
    self.cursor = self.database.cursor()
    
  def __del__(self):
    self.logger.debug("Destructor called, cleaning up")
    if self.cursor:
      self.cursor.close()
    if self.database:
      self.database.close()
    
  def setup(self):
    self.logger.debug("Creating items table if it doesn't exist")
    self.cursor.execute("""CREATE TABLE IF NOT EXISTS items (
    data_id INTEGER NOT NULL PRIMARY KEY, 
    name TEXT DEFAULT NONE, 
    level INTEGER DEFAULT 0, 
    rarity_word TEXT DEFAULT NONE, 
    rarity INTEGER DEFAULT 0,  
    description TEXT DEFAULT NONE,
    gem_store_description TEXT DEFAULT NONE, 
    gem_store_blurb TEXT DEFAULT NONE,
    img TEXT DEFAULT NONE);""")
    
    self.logger.debug("Creating prices table if it doesn't exist")
    self.cursor.execute("""CREATE TABLE IF NOT EXISTS prices (
    data_id INTEGER, 
    vendor_sell_price INTEGER,
    min_sale_unit_price INTEGER NOT NULL ON CONFLICT FAIL, 
    max_offer_unit_price INTEGER NOT NULL ON CONFLICT FAIL, 
    sale_availability INTEGER NOT NULL ON CONFLICT FAIL, 
    offer_availability INTEGER NOT NULL ON CONFLICT FAIL, 
    timestamp LONG, 
    FOREIGN KEY (data_id) REFERENCES items(data_id));""")
  
  def total_items(self):
    self.logger.info("Fetching total number of items in database")
    self.cursor.execute("SELECT count(data_id) FROM items;")
    return self.cursor.fetchone()[0]
  
  # internal add_item function, takes a dict in the same format as a search result dict
  def _add_item(self, the_info):
    if not isinstance(the_info, dict): 
      self.logger.error("Adding an item requires a dict as the argument")
      return
    
    # default values
    info = {"name" : None,
            "level" : None,
            "rarity_word" : None,
            "rarity" : None,
            "vendor_sell_price" : None,
            "min_sale_unit_price" : None,
            "max_offer_unit_price" : None,
            "sale_availability" : None,
            "offer_availability" : None,
            "description" : None,
            "gem_store_description" : None,
            "gem_store_blurb" : None,
            "img" : None,
            "timestamp" : time.time()}
    
    info.update(the_info)
    
    self.logger.info("Adding item to database with id %s and name %s" % (info["data_id"], info["name"]))
    self.cursor.execute("""INSERT OR REPLACE INTO items VALUES (:data_id, :name, :level, 
    :rarity_word, :rarity, :description, :gem_store_description, :gem_store_blurb, :img);""", info)
    self.database.commit()
    self.cursor.execute("""INSERT OR IGNORE INTO prices VALUES (:data_id, :vendor_sell_price, 
    :min_sale_unit_price, :max_offer_unit_price, :sale_availability, :offer_availability, :timestamp);""", info)
    self.database.commit()
  
  # manual add_item function, shouldnt need to be used since we will most likely 
  # pull from trading post and input directly to database
  def add_item(self, data_id, name=None, level=0, rarity_word=None, rarity=0, vendor_sell_price=0,
               min_sale_unit_price=0, max_offer_unit_price=0, sale_availability=0, offer_availability=0,
               description=None, img=None):
    
    info = {"data_id" : data_id,
            "name" : name,
            "level" : level,
            "rarity_word" : rarity_word,
            "rarity" : rarity,
            "vendor_sell_price" : vendor_sell_price,
            "min_sale_unit_price" : min_sale_unit_price,
            "max_offer_unit_price" : max_offer_unit_price,
            "sale_availability" : sale_availability,
            "offer_availability" : offer_availability,
            "description" : description,
            "gem_store_description" : gem_store_description,
            "gem_store_blurb" : gem_store_blurb,
            "img" : img,
            "timestamp" : time.time()}
    
    self.logger.info("Adding item to database with id %s and name %s" % (info["data_id"], info["name"]))
    self.cursor.execute("""INSERT OR REPLACE INTO items VALUES (:data_id, :name, :level, 
    :rarity_word, :rarity, :description, :gem_store_description, :gem_store_blurb, :img);""", info)
    self.database.commit()
    self.cursor.execute("""INSERT OR IGNORE INTO prices VALUES (:data_id, :vendor_sell_price, 
    :min_sale_unit_price, :max_offer_unit_price, :sale_availability, :offer_availability, :timestamp);""", info)
    self.database.commit()
    
  def add_items(self, start=None, limit=10, delay=5):
    self.logger.info("Preparing to add items to the database")
    tp = self.prepare_trading_post()
    
    if start is None:
      self.cursor.execute("SELECT max(data_id) FROM items;")
      start = self.cursor.fetchone()[0]
      
      if start is None: start = 1
    
    self.logger.info("Starting from %s" % start)
    self.logger.debug("Limit %u item(s)" % limit)
    limit += start
    self.logger.debug("Stopping before %u" % limit)
    
    for i in range(start, limit):
      item = tp.search_id(i)['results'][0]
      self._add_item(item)
      if limit-start > 10 and i % 10 == 0: 
        self.logger.debug("Sleeping for %u second(s)" % delay)
        time.sleep(delay)
    self.logger.info("There are %u items in the database" % self.total_items())
    
  def _add_item_price(self,the_info):
    if not isinstance(the_info, dict): 
      self.logger.error("Adding an item's price requires a dict as the argument")
      return
    
    # default values
    info = {"vendor_sell_price" : None,
            "min_sale_unit_price" : None,
            "max_offer_unit_price" : None,
            "sale_availability" : None,
            "offer_availability" : None,
            "timestamp" : time.time()}
    
    info.update(the_info)
    
    self.logger.info("Updating price in database for item with id %s" % info['data_id'])
    self.cursor.execute("""INSERT OR IGNORE INTO prices VALUES (:data_id, :vendor_sell_price, 
    :min_sale_unit_price, :max_offer_unit_price, :sale_availability, :offer_availability, :timestamp);""", info)
    self.database.commit()
    
    
  def add_item_prices(self, delay=5):
    self.logger.info("Preparing to add item prices to the database")
    tp = self.prepare_trading_post()
    
    self.cursor.execute("SELECT count(DISTINCT data_id),min(data_id) FROM prices;")
    limit, start = self.cursor.fetchone()
    
    if start is None or limit is None:
      self.logger.error("There doesn't appear to be any entries in the prices database, nothing to add")
      return False
    
    self.logger.info("Starting from %s" % start)
    self.logger.debug("Limit %u" % limit)
    limit += start
    
    for i in range(start, limit):
      item = tp.search_id(i)['results'][0]
      self._add_item_price(item)
      if limit-start > 10 and i % 10 == 0: 
        self.logger.debug("Sleeping for %u second(s)" % delay)
        time.sleep(delay)
    
  def prepare_trading_post(self):
    gw = GuildWars2()
    self.logger.debug("Logging in to Guild Wars 2")
    gw.login(account['login'])
    valid, name = gw.is_session_valid()
    
    if valid:
      self.logger.debug("Session is valid, logged in as %s" % name)
      tp = TradingPost()
      if tp.authenticate():
        self.logger.debug("Successfully authenticated on the Trading Post")
        return tp
    return False
  
  def dump_to_file_sql(self):
    filename = "dumps/Database.sql"
    self.logger.info("Dumping database to %s" % filename)
    with open(filename, 'w') as f:
      for line in self.database.iterdump():
        f.write('%s\n' % line)
    f.close()
    self.logger.info("Dump complete!")
    
  def dump_to_file_html(self, table_name):
    filename = "dumps/" + table_name + ".html"
    self.logger.info("Dumping table %s to %s" % (table_name,filename))
    
    f = open(filename, "w")
    f.write("<html>\n<body>\n<h1>%s</h1>\n" % table_name)
    
    res = self.cursor.execute("PRAGMA table_info('%s')" % table_name)
    column_names = [str(table_info[1]) for table_info in res.fetchall()]
    
    f.write("<table border=1>\n\t<thead>\n\t\t<tr>")
    f.write("".join(["<td>" + str(c) + "</td>" for c in column_names]))
    f.write("</tr>\n\t</thead>\n\t<tbody>\n")
    
    q = "SELECT "
    q += ",".join([col for col in column_names])
    q += " FROM %(tbl_name)s"
    
    query_res = self.cursor.execute(q % {'tbl_name': table_name})
    for row in query_res:
      f.write("\t\t<tr>")
      f.write("".join(["<td>" + str(c) + "</td>" for c in row]))
      f.write("</tr>\n")
    f.write("\t</tbody>\n</table>\n</body>\n</html>")
    
    f.close()
    self.logger.info("Dump complete!")
    
  def dump_to_file_csv(self, table_name):
    filename = "dumps/" + table_name + ".csv"
    self.logger.info("Dumping table %s to %s" % (table_name,filename))
    
    f = open(filename, "w")
    
    res = self.cursor.execute("PRAGMA table_info('%s')" % table_name)
    column_names = [str(table_info[1]) for table_info in res.fetchall()]
    
    f.write("".join([str(c) + "," for c in column_names]) + "\n")
    
    q = "SELECT "
    q += ",".join([col for col in column_names])
    q += " FROM %(tbl_name)s"
    query_res = self.cursor.execute(q % {'tbl_name': table_name})
    
    for row in query_res:
      f.write("".join([str(c) + "," for c in row]) + "\n")

    f.close()
    self.logger.info("Dump complete!")
  
if __name__ == "__main__":
  bldb = BlackLionDatabase()
  bldb.setup()
#  bldb.add_items(start=80,limit=10,delay=10)
#  bldb.add_item_prices(delay=10)
  bldb.dump_to_file_csv("items")
  bldb.dump_to_file_csv("prices")
#  bldb.dump_to_file_html("items")
#  bldb.dump_to_file_sql()
  
